<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisaProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        Schema::create('visa_profiles', function (Blueprint $table) {
//            $table->increments('id');
//            $table->unsignedInteger('user_id');
//            $table->string('reference_id', 20);
//            $table->string('customer_profile_id');
//            $table->string('payment_profile_id');
//            $table->timestamps();
//
//            $table->foreign('user_id')->references('id')->on('users');
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visa_profile');
    }
}
