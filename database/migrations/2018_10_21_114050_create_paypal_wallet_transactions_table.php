<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaypalWalletTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paypal_wallet_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('paypal_wallet_id');
            $table->string('payment_id');
            $table->string('token');
            $table->string('payer_id');
            $table->timestamps();

            $table->foreign('paypal_wallet_id')->references('id')->on('paypal_wallets');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paypal_wallet_transaction');
    }
}
