<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisaTokenSystemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visa_token_system', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tokenized_card_id');
            $table->string('card_number');
            $table->string('card_exp_date');
            $table->string('currency_code');
            $table->string('acceptor_name');
            $table->string('acceptor_state');
            $table->string('acceptor_country');
            $table->integer('acceptor_country_code');
            $table->string('acceptor_zip_code');
            $table->integer('acquiring_bin');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visa_token_system');
    }
}
