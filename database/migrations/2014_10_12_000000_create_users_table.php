<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('visa_token_system_id')->nullable();
            $table->string('profile_type');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('uuid')->nullable();
            $table->string('email')->unique();
            $table->timestamps();

            $table->foreign('visa_token_system_id')->references('id')->on('visa_token_system');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
