<?php

use App\Models\VisaTokenSystem;
use Illuminate\Database\Seeder;
use Ramsey\Uuid\Uuid;

class VisaTokenSystemSeeder extends Seeder
{
    /**
     * @throws Exception
     */
    public function run()
    {
        //This is mimic'ing Visas's VTS (Since we don't have access to it)

        //This is the Buyers Credit Card Info
        (new VisaTokenSystem([
            'id'                    => 1,
            'tokenized_card_id'     => (string)UUID::uuid4(),
            'card_number'           => '4957030005123304',
            'card_exp_date'         => '2020-03',
            'currency_code'         => 'USD',
            'acceptor_name'         => 'Acceptor 1',
            'acceptor_state'        => 'CA',
            'acceptor_country'      => 'USA',
            'acceptor_country_code' => 840,
            'acceptor_zip_code'     => '94404',
            'acquiring_bin'         => '408999',
        ]))->save();

        //This is the Merchants Credit Card Info
        (new VisaTokenSystem([
            'id'                    => 2,
            'tokenized_card_id'     => (string)UUID::uuid4(),
            'card_number'           => '4895142232120006',
            'card_exp_date'         => '2020-03',
            'currency_code'         => 'USD',
            'acceptor_name'         => 'Acceptor 9',
            'acceptor_state'        => 'CA',
            'acceptor_country'      => 'USA',
            'acceptor_country_code' => 840,
            'acceptor_zip_code'     => '94404',
            'acquiring_bin'         => '408999',
        ]))->save();
    }
}
