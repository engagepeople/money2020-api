<?php

use App\Models\PayPalWallet;
use Illuminate\Database\Seeder;

class PayPalWalletSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        (new PayPalWallet([
            'user_id'         => 3,
            'running_balance' => 500.00, //Giving a $500 sign up bonus :)
        ]))->save();
    }
}
