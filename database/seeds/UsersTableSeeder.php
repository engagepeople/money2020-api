<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // This is the Visa Buyer
        (new User([
            'id'                   => 1,
            'profile_type'         => User::ACCOUNT_VISA,
            'visa_token_system_id' => 1,
            'first_name'           => 'Ivan',
            'last_name'            => 'Carosati',
            'email'                => 'icarosati@engagepeople.com',
        ]))->save();

        // This is the Merchant
        (new User([
            'id'                   => 2,
            'profile_type'         => User::ACCOUNT_VISA,
            'visa_token_system_id' => 2,
            'first_name'           => 'Coffee',
            'last_name'            => 'Machine',
            'email'                => 'sdealmeida@engagepeople.com',
        ]))->save();

        // This is the PayPal Buyer
        (new User([
            'id'                   => 3,
            'profile_type'         => User::ACCOUNT_PAYPAL,
            'first_name'           => 'Alessandro',
            'last_name'            => 'Andriani',
            'email'                => 'ale@rawinc.com',
        ]))->save();
    }
}
