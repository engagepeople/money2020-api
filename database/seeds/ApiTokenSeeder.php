<?php

use App\Models\ApiToken;
use Illuminate\Database\Seeder;

class ApiTokenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // This is the Visa Buyers Token
        (new Apitoken([
            'user_id' => 1,
            'token' => 'DNNMCNDA'
        ]))->save();

        // This is the Merchants Token
        (new Apitoken([
            'user_id' => 2,
            'token' => 'DEADBEEF'
        ]))->save();

        // This is the PayPal Buyers Token
        (new Apitoken([
            'user_id' => 3,
            'token' => 'PAYPALTK'
        ]))->save();
    }
}
