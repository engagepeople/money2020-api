<?php

namespace App\Jobs\Visa;

use App\Models\Order;
use App\Models\User;
use App\Models\VisaTokenSystem;
use App\Models\VisaTransaction;
use Carbon\Carbon;
use ft_100\api\Funds_transferApi;
use ft_100\Configuration;
use ft_100\model\Address;
use ft_100\model\CardAcceptor;
use ft_100\model\PullfundspostPayload;
use ft_100\model\PushfundspostPayload;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ChargeUserJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $orderId;

    private $client;

    public function __construct(int $orderId)
    {
        $this->orderId = $orderId;

        $this->client = Configuration::getDefaultConfiguration();
        $this->client->setUsername(env("VISA_API_USERNAME"));
        $this->client->setPassword(env("VISA_API_SECRET"));
        $this->client->setCertificatePath(env("VISA_CERT_PATH"));
        $this->client->setPrivateKey(env("VISA_PRIVATE_KEY"));
//        $this->client->setDebug(true);
    }

    public function handle()
    {
        /** @var Order $order */
        $order = Order::query()
            ->where('id', $this->orderId)
            ->first();

        // Charge the User
        $this->pullFunds($order);

        // Fund the Merchant
        $this->pushFunds($order);
    }

    private function pushFunds(Order $order)
    {
        $buyer = $order->user;
        $merchant = $order->merchant;

        $buyerTC = $buyer->tokenizedCard;
        $merchantTC = $merchant->tokenizedCard;

        //Instantiate Funds_transferApi
        $api_instance = new Funds_transferApi();

        // Set the required payload and parameters.
        $payload = new PushfundspostPayload();

        $payload->setBusinessApplicationId('PP');
        $payload->setSystemsTraceAuditNumber(451018);
        $payload->setRetrievalReferenceNumber('412770451018');
        $payload->setLocalTransactionDateTime(Carbon::now()->toIso8601String());


        $payload->setSenderAccountNumber($buyerTC->card_number);
        $payload->setSenderAddress("1600 Pennsylvania Avenue NW");
        $payload->setSenderCity('Washington');
        $payload->setSenderCountryCode($buyerTC->acceptor_country_code);
        $payload->setSenderName($buyerTC->acceptor_name);
        $payload->setSenderReference($order->order_number); // Check
        $payload->setSenderStateCode($buyerTC->acceptor_state);

        $payload->setAcquiringBin($merchantTC->acquiring_bin);
        $payload->setAcquirerCountryCode($merchantTC->acceptor_country_code);
        $payload->setRecipientName($merchantTC->acceptor_name);
        $payload->setRecipientPrimaryAccountNumber($merchantTC->card_number);

        $payload->setTransactionCurrencyCode($merchantTC->currency_code);
        $payload->setAccountType('10'); // Account type is checking
        $payload->setAmount((float)$order->amount);

        $cardAcceptor = new CardAcceptor();
        $cardAcceptor->setName('Visa Inc. USA-Foster City');
        $cardAcceptor->setTerminalId('TID-9999');
        $cardAcceptor->setIdCode('CA-IDCode-77765');

        $address = new Address();
        $address->setState($merchantTC->acceptor_state);
        $address->setCountry($merchantTC->acceptor_country);
        $address->setCounty('San Mateo');
        $address->setZipCode($merchantTC->acceptor_zip_code);

        $cardAcceptor->setAddress($address);

        $payload->setCardAcceptor($cardAcceptor);

        try {
            $result = $api_instance->postpushfunds($payload);

            // If this didn't fail, then we can assume that the Action Code is 00
            // I know that the action code returned doesn't have to be 00, but for the
            // cc I chose, I know that it will return 00
            $transaction = new VisaTransaction([
                'approval_code'                               => $result->getApprovalCode(),
                'transaction_identifier'                      => $result->getTransactionIdentifier(),
                'response_code'                               => $result->getResponseCode(),
                'action_code'                                 => $result->getActionCode(),
                'transmission_date_time'                      => $result->getTransmissionDateTime(),
            ]);
            $transaction->user()->associate($merchant);
            $transaction->save();

            $order->update(['order_status' => Order::ORDER_PAID]);
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            $order->update(['order_status' => Order::ORDER_ERROR]);
        }
    }

    /**
     * @param Order $order
     */
    private function pullFunds(Order $order)
    {
        /** @var User $buyer */
        $buyer = $order->user;

        // Imagine this is Visas Tokenized Card
        /** @var VisaTokenSystem $tokenizedCard */
        $tokenizedCard = $buyer->tokenizedCard;

        //Instantiate Funds_transferApi
        $api_instance = new Funds_transferApi();

        // Set the required payload and parameters.
        $payload = new PullfundspostPayload();
        $payload->setSystemsTraceAuditNumber(121212);
        $payload->setRetrievalReferenceNumber('330000550000');
        $payload->setLocalTransactionDateTime(Carbon::now()->toIso8601String());
        $payload->setBusinessApplicationId('PP');
        $payload->setAmount($order->amount);

        $payload->setAcquiringBin($tokenizedCard->acquiring_bin);
        $payload->setAcquirerCountryCode($tokenizedCard->acceptor_country_code);
        $payload->setSenderPrimaryAccountNumber($tokenizedCard->card_number);
        $payload->setSenderCardExpiryDate($tokenizedCard->card_exp_date);
        $payload->setSenderCurrencyCode($tokenizedCard->currency_code);

        $cardAcceptor = new CardAcceptor();
        $cardAcceptor->setName('Visa Inc. USA-Foster City');
        $cardAcceptor->setTerminalId('ABCD1234');
        $cardAcceptor->setIdCode('ABCD1234ABCD123');

        $address = new Address();
        $address->setState($tokenizedCard->acceptor_state);
        $address->setCountry($tokenizedCard->acceptor_country);
        $address->setZipCode($tokenizedCard->acceptor_zip_code);

        $cardAcceptor->setAddress($address);

        $payload->setCardAcceptor($cardAcceptor);

        try {
            $result = $api_instance->postpullfunds($payload);
            $transaction = new VisaTransaction([
                'approval_code'                               => $result->getApprovalCode(),
                'transaction_identifier'                      => $result->getTransactionIdentifier(),
                'response_code'                               => $result->getResponseCode(),
                'action_code'                                 => $result->getActionCode(),
                'cps_authorization_characteristics_indicator' => $result->getCpsAuthorizationCharacteristicsIndicator(),
                'transmission_date_time'                      => $result->getTransmissionDateTime(),
            ]);
            $transaction->user()->associate($buyer);
            $transaction->save();
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            $order->update(['order_status' => Order::ORDER_ERROR]);
        }
    }
}
