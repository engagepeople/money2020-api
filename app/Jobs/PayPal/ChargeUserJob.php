<?php

namespace App\Jobs\PayPal;

use App\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ChargeUserJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $orderId;

    private $client;

    public function __construct(int $orderId)
    {
        $this->orderId = $orderId;
    }

    public function handle()
    {
        /** @var Order $order */
        $order = Order::query()
            ->where('id', $this->orderId)
            ->first();

        $user = $order->user;
        $paypalWallet = $user->paypalWallet;

        $paypalWallet->update([
            'running_balance' => $paypalWallet->running_balance - $order->amount
        ]);

        $order->update(['order_status' => Order::ORDER_PAID]);
    }
}
