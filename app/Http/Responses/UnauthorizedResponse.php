<?php

namespace App\Http\Responses;


use Illuminate\Http\JsonResponse;

class UnauthorizedResponse extends JsonResponse
{
    const UNAUTHORIZED = "UNAUTHORIZED";

    public function __construct()
    {
        $response = [
            'status' => false,
            'error'  => self::UNAUTHORIZED,
        ];

        parent::__construct($response, $this::HTTP_UNAUTHORIZED);
    }
}
