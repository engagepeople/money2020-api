<?php

namespace App\Http\Controllers\Order;

use App\Http\Controllers\Controller;
use App\Jobs;
use App\Models\Order;
use App\Models\User;
use App\Providers\Money2020Provider;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Ramsey\Uuid\Uuid;

class OrderController extends Controller
{
    /** @var Money2020Provider */
    private $money2020Provider;

    public function __construct(Money2020Provider $money2020Provider)
    {
        $this->money2020Provider = $money2020Provider;
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function createOrder(Request $request)
    {
        /** @var User $merchant */
        $merchant = $this->money2020Provider->user;

        $orderNumber = substr(str_replace('-', '', (string)UUID::uuid4()), 0, 10);
        $order = new Order([
            'order_number' => $orderNumber,
            'order_status' => Order::ORDER_CREATED,
            'amount'       => $request->input('amount'),
        ]);
        $order->merchant()->associate($merchant);
        $order->save();

        return response([
            'message' => 'Order created successfully.',
            'id'      => $order->order_number,
        ]);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function attachOrder(Request $request)
    {
        /** @var User $user */
        $user = $this->money2020Provider->user;

        /** @var Order $order */
        $order = Order::query()
            ->where('order_number', $request->input('order_number'))
            ->first();

        if ($order != null) {
            $order->user()->associate($user);
            $order->update([
                'order_status' => Order::ORDER_ACCEPTED,
            ]);

            // Charge the customer
            // I would put this in a Queue but since we are at the hackathon
            // anything can go wrong. So I'll use the sync driver instead.
            if ($user->profile_type === User::ACCOUNT_VISA) {
                $this->dispatch(new Jobs\Visa\ChargeUserJob($order->id));
            } else {
                $this->dispatch(new Jobs\PayPal\ChargeUserJob($order->id));
            }

            return response([
                'message' => 'Order accepted successfully.',
                'id'      => $order->order_number,
            ], Response::HTTP_OK);
        }

        return response([
            'message' => 'Order not found.',
            'id'      => $request->input('order_number'),
        ], Response::HTTP_NOT_FOUND);
    }

    public function orderStatus(Request $request)
    {
        /** @var Order $order */
        $order = Order::query()
            ->where('order_number', $request->input('order_number'))
            ->first();

        if ($order != null) {
            $response = response([
                'order_number' => $order->order_number,
                'order_status' => $order->order_status,
            ], Response::HTTP_OK);

            if ($order->order_status === Order::ORDER_PAID) {
                $order->update([
                    'order_status' => ORder::ORDER_PROCESSED
                ]);
            }

            return $response;
        }

        return response([
            'message' => 'Order not found.',
            'id'      => $request->input('order_number'),
        ], Response::HTTP_NOT_FOUND);
    }
}
