<?php

namespace App\Http\Controllers\Visa;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\VisaTransaction;
use App\Providers\Money2020Provider;
use Carbon\Carbon;
use ft_100\api\Funds_transferApi;
use ft_100\Configuration;
use ft_100\model\Address;
use ft_100\model\CardAcceptor;
use ft_100\model\PullfundspostPayload;
use ft_100\model\PushfundspostPayload;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class VisaController extends Controller
{
    const PULL_TRANSACTION_URL = 'https://sandbox.api.visa.com/visadirect/fundstransfer/v1/pullfundstransactions';

    /** @var Money2020Provider */
    private $serviceProvider;

    private $client;

    public function __construct(Money2020Provider $serviceProvider)
    {
        $this->serviceProvider = $serviceProvider;

        $this->client = Configuration::getDefaultConfiguration();
        $this->client->setUsername(env("VISA_API_USERNAME"));
        $this->client->setPassword(env("VISA_API_SECRET"));
        $this->client->setCertificatePath(env("VISA_CERT_PATH"));
        $this->client->setPrivateKey(env("VISA_PRIVATE_KEY"));
//        $this->client->setDebug(true);
    }

    public function pullFunds(Request $request)
    {

        /** @var User $user */
        $user = $this->serviceProvider->user;

        //Instantiate Funds_transferApi
        $api_instance = new Funds_transferApi();

        // Set the required payload and parameters.
        $payload = new PullfundspostPayload();
        $payload->setSystemsTraceAuditNumber(121212);
        $payload->setRetrievalReferenceNumber('330000550000');
        $payload->setLocalTransactionDateTime(Carbon::now()->toIso8601String());
        $payload->setAcquiringBin(408999);
        $payload->setAcquirerCountryCode(840);
        $payload->setSenderPrimaryAccountNumber('4957030005123304');
        $payload->setSenderCardExpiryDate('2020-03');
        $payload->setSenderCurrencyCode('USD');
        $payload->setAmount(10.10);
        $payload->setBusinessApplicationId('PP');

        $cardAcceptor = new CardAcceptor();
        $cardAcceptor->setName('Visa Inc. USA-Foster City');
        $cardAcceptor->setTerminalId('ABCD1234');
        $cardAcceptor->setIdCode('ABCD1234ABCD123');

        $address = new Address();
        $address->setState('CA');
        $address->setCountry('USA');
        $address->setZipCode('94404');

        $cardAcceptor->setAddress($address);

        $payload->setCardAcceptor($cardAcceptor);

        try {
            $result = $api_instance->postpullfunds($payload);

            $transaction = new VisaTransaction([
                'approval_code'                               => $result->getApprovalCode(),
                'transaction_identifier'                      => $result->getTransactionIdentifier(),
                'response_code'                               => $result->getResponseCode(),
                'action_code'                                 => $result->getActionCode(),
                'cps_authorization_characteristics_indicator' => $result->getCpsAuthorizationCharacteristicsIndicator(),
                'transmission_date_time'                      => $result->getTransmissionDateTime(),
            ]);
            $transaction->user()->associate($user);
            $transaction->save();

            return response([
                'message' => 'Successfully created pull transaction.',
            ], Response::HTTP_CREATED);

        } catch (\Exception $e) {
            echo 'Exception when calling Funds_transferApi->postpullfunds: ', $e->getMessage(), PHP_EOL;
        }
    }

    public function pushFunds(Request $request)
    {
        $user = $this->serviceProvider->user;

        // Configure HTTP basic authorization: basicAuth

        //Instantiate Funds_transferApi
        $api_instance = new Funds_transferApi();

        // Set the required payload and parameters.
        $payload = new PushfundspostPayload();
        $payload->setSystemsTraceAuditNumber(451018);
        $payload->setRetrievalReferenceNumber('412770451018');
        $payload->setSenderReference("");
        $payload->setLocalTransactionDateTime(Carbon::now()->toIso8601String());
        $payload->setAcquiringBin(408999);

        $payload->setAcquirerCountryCode(840);
        $payload->setSenderCountryCode(840);

        $payload->setSenderName("Simon de Almeida");
        $payload->setSenderAddress("1600 Pennsylvania Avenue NW");
        $payload->setSenderCity('Washington');
        $payload->setSenderStateCode("DC");
        $payload->setSenderPostalCode('20500');
        $payload->setSenderFirstName('Simon');
        $payload->setSenderMiddleInitial("");
        $payload->setSenderLastName("Almeida");

        //$payload->setSenderDateOfBirth("01011990");

        $payload->setSenderAccountNumber('4957030005123304');

        $payload->setRecipientName('Ivan Carosati');
        $payload->setRecipientState('DC');
        $payload->setRecipientCountryCode('USA');
//        $payload->recipientCity()
        $payload->setRecipientFirstName('Ivan');
        $payload->setRecipientLastName('Carosati');
        $payload->setRecipientMiddleInitial("");
        $payload->setRecipientPrimaryAccountNumber('');
        $payload->setRecipientPrimaryAccountNumber('4957030420210454');
        $payload->setRecipientCardExpiryDate('2020-01');


        $payload->setTransactionCurrencyCode('USD');
        $payload->setAccountType('10'); // Savings Account
        $payload->setAmount(10.10);
//        'transactionFeeAmt',
        //$payload->setSurcharge('0.00');
        //$payload->setNationalReimbursementFee(0.00);

        $payload->setBusinessApplicationId('PP');

        $cardAcceptor = new CardAcceptor();
        $cardAcceptor->setName('Visa Inc. USA-Foster City');
        $cardAcceptor->setTerminalId('TID-9999');
        $cardAcceptor->setIdCode('CA-IDCode-77765');

        $address = new Address();
        $address->setState('CA');
        $address->setCountry('USA');
        $address->setCounty('San Mateo');
        $address->setZipCode('94404');

        $cardAcceptor->setAddress($address);

        $payload->setCardAcceptor($cardAcceptor);

        try {
            $result = $api_instance->postpushfunds($payload);
            print_r($result);
        } catch (\Exception $e) {
            echo 'Exception when calling Funds_transferApi->postpushfunds: ', $e->getMessage(), PHP_EOL;
        }
    }
}
