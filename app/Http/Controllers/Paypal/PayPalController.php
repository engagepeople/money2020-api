<?php

namespace App\Http\Controllers\Paypal;

use App\Http\Controllers\Controller;
use App\Models\PayPalWalletTransaction;
use App\Models\User;
use Illuminate\Http\Request;
use PayPal\Api\Amount;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;

class PayPalController extends Controller
{

    /** @var \PayPal\Rest\ApiContext */
    private $apiContext;

    public function __construct()
    {
        $this->apiContext = $apiContext = new \PayPal\Rest\ApiContext(
            new \PayPal\Auth\OAuthTokenCredential(
                env('PAYPAL_CLIENT_ID'),
                env('PAYPAL_CLIENT_SECRET')
            )
        );
    }

    public function fundAccount(Request $request)
    {
        // Create new payer and method
        $payer = new Payer();
        $payer->setPaymentMethod("paypal");

        // Set redirect urls
        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl(env('APP_URL') . "/v1/paypal/process")
            ->setCancelUrl(env('APP_URL') . "/cancel");

        // Set payment amount
        $amount = new Amount();
        $amount->setCurrency("USD")
            ->setTotal(10);

        // Set transaction object
        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setDescription("$10 Funded to your account");

        // Create the full payment object
        $payment = new Payment();
        $payment->setIntent("sale")
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions(array($transaction));

        try {
            $payment->create($this->apiContext);
            return redirect($payment->getApprovalLink());
        } catch (\PayPal\Exception\PayPalConnectionException $ex) {
            // This will print the detailed information on the exception.
            //REALLY HELPFUL FOR DEBUGGING
            echo $ex->getData();
        }

    }

    public function process(Request $request)
    {
        // I don't have access to the users Token here
        // I wouldn't code this, but hey, this is a hackathon
        /** @var User $user */
        $user = User::query()
            ->where('id', 3)
            ->first();

        $paypalTransaction = new PayPalWalletTransaction([
            'payment_id' => $request->input('paymentId'),
            'token'      => $request->input('token'),
            'payer_id'   => $request->input('PayerID'),
        ]);
        $paypalTransaction->wallet()->associate($user->paypalWallet);
        $paypalTransaction->save();

        // Get payment object by passing paymentId
        $paymentId = $paypalTransaction->payment_id;
        $payment = Payment::get($paymentId, $this->apiContext);
        $payerId = $paypalTransaction->payer_id;

        // Execute payment with payer ID
        $execution = new PaymentExecution();
        $execution->setPayerId($payerId);

        try {
            // Execute payment
            $result = $payment->execute($execution, $this->apiContext);

            $user->paypalWallet()->update([
                'running_balance' => $user->paypalWallet->running_balance + 10,
            ]);

            return view('paypal');
        } catch (\PayPal\Exception\PayPalConnectionException $ex) {
            echo $ex->getCode();
            echo $ex->getData();
            die($ex);
        } catch (\Exception $ex) {
            die($ex);
        }
    }
}
