<?php

namespace App\Http\Middleware;

use App\Http\Responses\UnauthorizedResponse;
use App\Models\ApiToken;
use App\Providers\Money2020Provider;
use Illuminate\Http\Request;

/**
 * Class Money2020Auth
 * @package App\Http\Middleware
 */
class Money2020Auth
{
    /**
     * @var Money2020Provider $money2020
     */
    private $money2020;

    /**
     * Money2020Auth constructor.
     *
     * @param Money2020Provider $money2020
     */
    public function __construct(Money2020Provider $money2020)
    {
        $this->money2020 = $money2020;
    }

    /**
     * @param Request  $request
     * @param \Closure $next
     *
     * @return mixed
     */
    public function handle(Request $request, \Closure $next)
    {
        $authHeader = $request->header('Auth');

        if ($authHeader == null) {
            return new UnauthorizedResponse();
        }

        $apiToken = ApiToken::query()
            ->where('token', $authHeader)
            ->with(['user'])
            ->take(1)
            ->first();

        if ($apiToken == null || $apiToken->user == null) {
            return new UnauthorizedResponse();
        }

        $this->money2020->user = $apiToken->user;
        $this->money2020->apiToken = $apiToken;

        return $next($request);
    }
}
