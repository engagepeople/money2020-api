<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class VisaTransaction
 * @package App\Models
 *
 * @property-read  int    $id
 * @property       string $tokenized_card
 * @property       string $card_number
 * @property       string $card_exp_date
 * @property       string $currency_code
 * @property       string $acceptor_name
 * @property       string $acceptor_state
 * @property       string $acceptor_country
 * @property       string $acceptor_zip_code
 * @property       int    $acquiring_bin
 * @property       int    $acceptor_country_code
 * @property-read  Carbon $create_at
 * @property-read  Carbon $updated_at
 *
 */
class VisaTokenSystem extends Model
{
    protected $table = 'visa_token_system';

    protected $fillable = [
        'id',
        'tokenized_card_id',
        'card_number',
        'card_exp_date',
        'currency_code',
        'acceptor_state',
        'acceptor_country',
        'acceptor_zip_code',
        'acquiring_bin',
        'acceptor_country_code',
    ];
}

