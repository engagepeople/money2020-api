<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class VisaTransaction
 * @package App\Models
 */
class VisaTransaction extends Model
{
    protected $fillable = [
        'approval_code',
        'transaction_identifier',
        'response_code',
        'action_code',
        'cps_authorization_characteristics_indicator',
        'transmission_date_time',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
