<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class User
 * @package App\Models
 *
 * @property-read int             $id
 * @property      string          $profile_type
 * @property      string          $first_name
 * @property      string          $last_name
 * @property      string          $uuid
 * @property      string          $email
 * @property-read Carbon          $created_at
 * @property-read Carbon          $updated_at
 * @property-read VisaTokenSystem $tokenizedCard
 * @property-read PayPalWallet    $paypalWallet
 */
class User extends Model
{
    const ACCOUNT_VISA = 'ACCOUNT_VISA';
    const ACCOUNT_PAYPAL = 'ACCOUNT_PAYPAL';

    protected $fillable = [
        'id',
        'profile_type',
        'first_name',
        'last_name',
        'uuid',
        'email',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function paypalWallet()
    {
        return $this->hasOne(
            PayPalWallet::class,
            'user_id'
        );
    }

    /**
     * This is mimic'ing visa's VTS
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tokenizedCard()
    {
        return $this->belongsTo(
            VisaTokenSystem::class,
            'visa_token_system_id'
        );
    }

}
