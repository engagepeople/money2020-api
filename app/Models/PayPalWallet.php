<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PayPalWallet
 * @package App\Models
 *
 * @property-read int   $id
 * @property      int   $user_id
 * @property      float $running_balance
 * @property-read User  $user
 */
class PayPalWallet extends Model
{
    protected $table = 'paypal_wallets';

    protected $fillable = [
        'user_id',
        'running_balance',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function transactions()
    {
        return $this->hasMany(
            PayPalWalletTransaction::class
        );
    }
}
