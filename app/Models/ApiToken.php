<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ApiToken
 * @package App\Models
 *
 * @property-read int    $user_id
 * @property      string $token
 * @property-read Carbon $created_at
 * @property-read Carbon $updated_at
 */
class ApiToken extends Model
{
    protected $fillable = [
        'id',
        'user_id',
        'token',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
