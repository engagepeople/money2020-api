<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Order
 * @package App\Models
 *
 * @property-read int    $id
 * @property-read int    $buyer_id
 * @property-read int    $merchant_id
 * @property      string $order_number
 * @property      string $order_status
 * @property      float  $amount
 * @property      Carbon $created_at
 * @property      Carbon $updated_at
 * @property-read User   $user
 * @property-read User   $merchant
 */
class Order extends Model
{
    const ORDER_CREATED = 'ORDER_CREATED';
    const ORDER_ACCEPTED = 'ORDER_ACCEPTED';
    const ORDER_CANCELLED = 'ORDER_CANCELLED';
    const ORDER_PAID = 'ORDER_PAID';
    const ORDER_PROCESSED = 'ORDER_PROCESSED';
    const ORDER_ERROR = 'ORDER_ERROR';

    protected $fillable = [
        'order_number',
        'order_status',
        'amount',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(
            User::class,
            'buyer_id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function merchant()
    {
        return $this->belongsTo(
            User::class,
            'merchant_id'
        );
    }
}
