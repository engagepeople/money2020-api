<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class PayPalWalletTransaction
 * @package App\Models
 *
 * @property-read int    $id
 * @property      int    $paypal_wallet_id
 * @property      string $payment_id
 * @property      string $token
 * @property      string $payer_id
 */
class PayPalWalletTransaction extends Model
{

    protected $table = 'paypal_wallet_transactions';

    protected $fillable = [
        'payment_id',
        'token',
        'payer_id',
    ];

    public function wallet()
    {
        return $this->belongsTo(
            PayPalWallet::class,
            'paypal_wallet_id'
        );
    }
}
