<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * Class Money2020ServiceProvider
 * @package App\Providers
 */
class Money2020ServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Money2020Provider::class, function($app) {
            return new Money2020Provider();
        });
    }

    /**
     * @return array
     */
    public function provides()
    {
        return [
            Money2020Provider::class,
        ];
    }
}
