<?php

namespace App\Providers;

use App\Models\ApiToken;
use App\Models\User;

/**
 * Class Money2020Provider
 * @package App\Providers
 */
class Money2020Provider
{
    /**
     * @var User
     */
    public $user;

    /**
     * @var ApiToken
     */
    public $apiToken;
}
