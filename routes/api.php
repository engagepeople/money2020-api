<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/welcome', function () {
    return view('welcome');
});

Route::group(['prefix' => '/paypal'], function() {
    Route::get('/fundAccount', 'Paypal\PayPalController@fundAccount');
    Route::get('/process', 'Paypal\PayPalController@process');
});

// Orders Api
Route::group(['middleware' => 'money2020.auth', 'prefix' => '/order'], function () {
    Route::get('/','Order\OrderController@createOrder');
    Route::get('/status','Order\OrderController@orderStatus');
    Route::post('/', 'Order\OrderController@attachOrder');
});

Route::group(['middleware' => 'money2020.auth', 'prefix' => '/visa'], function () {
    Route::get('pullFunds', 'Visa\VisaController@pullFunds');
    Route::get('pushFunds', 'Visa\VisaController@pushFunds');
});
